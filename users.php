<?php
session_start();
	require_once('connection.php');
	
	class users{
		
		private $db;
		function __construct(){
			$this->db = new connection();
			$this->db = $this->db->dbConnect();
		}
		public function login($username, $password){
			$stmt = $this->db->prepare("select * from users where username=:uname and password=:pass");
			$stmt->bindParam(':uname', $username);
			$stmt->bindParam(':pass', $password);
			$stmt->execute();
			$results = $stmt->fetch();
			return $results;

		}
	}
	?>