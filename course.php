<?php
  session_start();
  if(!isset($_SESSION['userdata'])){
    header('location: login.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Dashboard page</title>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/bootstrap-theme.css" rel="stylesheet">
  <link href="assets/css/bootsrap-theme.min.css" rel="stylesheet">
  <link href="assets/css/js/npm.js" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Welcome to BIM Student</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
      <li><a href="dashboard.php">Home</a></li>
        
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['userdata']['username']; ?> <span class="caret"></span></a>
        <ul class="dropdown-menu">
        <li><a href="#">Setting</a></li>
        <li><a href="logout.php">logout</a></li>
        </ul>
      </li>
      </ul>
    </div><!-- /.navbar-collapse -->
    </div>
<h3>Course Cycle</h3>
<div class="coursecycle">
<div class="coursecycle_left">
<table class="table table-striped table-bordered table-hover" cellspacing="0" cellpadding="0">
  <tr>
    <th >First Semester</td>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td >ENG 201: English -    I </th>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 211: Computer Information System </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 212: Digital Logic Design </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MGT 201: Principles of Management </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MTH 201: Basic Mathematics </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>15</strong></td>
  </tr>
</table>
<table class="table table-striped table-bordered table-hover" cellspacing="0" cellpadding="0">
  <tr>
    <th>Second Semester      </td>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td >IT 213: Structured    Programming </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 214: Data Communication and Computer    Network </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MGT 204: Business Communications </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MTH 202: Discrete Mathematics </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>SOC 201: Sociology for Business </td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>15</strong></td>
  </tr>
</table>
<table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
  <tr>
    <th>Third Semester
      </td>
    </th>
    <th align="right">Credit Hours</th>
  </tr>
  <tr>
    <td>ACC 201: Financial Accounting</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 215: Web Technology - I</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 216: Java Programming - I</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 217: Computer Organization</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>STT 201: Business Statistics</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>15</strong></td>
  </tr>
</table>
<table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
  <tr>
    <th>Fourth Semester </td>
    </th>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td>ACC 202: Cost and    Management Accounting</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>ECO 201: Microeconomics</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 218: Data Structure and Algorithm with Java</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 219: Web Technology - II</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 220: Database Management System</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>15</strong></td>
  </tr>
</table>

<table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
  <tr>
    <th>Fifth Semester </th>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td>ECO 202:    Macroeconomics</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 221: Computer Graphics</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 222: Java Programming - II</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 223: Advanced Internet Working</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MKT 201: Fundamentals of Marketing</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>15</strong></td>
  </tr>
</table>
</div>
<div class="coursecycle_right">
<table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
  <tr>
    <th  >Sixth Semester
      </td>
    </th>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td>FIN 201: Business    Finance</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 224: Software Engineering</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 225: Computer Security and Cyber    Law</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 351: Summer Project</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MGT 202: Human Resource Management</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MGT 206: Business Environment in    Nepal</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>18</strong></td>
  </tr>
</table>
<table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
  <tr>
    <th  >Seventh Semeter
      </td>
    </th>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td>IT 226: Management    Information System</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 227: Object Oriented Analysis and    Design</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 228: Artificial Intelligence</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MGT 203: Organizational Behavior</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MGT 205: Operations Management</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>MGT 208: Business Strategy</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>18</strong></td>
  </tr>
</table>

<table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
  <tr>
    <th  >Eight Semester   </th>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td>IT 229: IT    Entrepreneurship and Supply Chain Management</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 230: Economics of Information and Communication</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>Elective I</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>Elective I</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 350: Internship</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td><strong>Total</strong></td>
    <td align="right"><strong>15</strong></td>
  </tr>
</table>
<table class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
  <tr>
    <th  >Elective </th>
    <th align="right" >Credit Hours</th>
  </tr>
  <tr>
    <td >IT 301: System Administration - Win NT</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 302: Graphics User Interface Programming Using C++</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 303: Computer Based Financial Engineering</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 304: Electronic Reporting and Auditing</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 305: Object Oriented Database Management</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 306: Software Project Management</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 307: Operating Systems</td>
    <td align="right">3</td>
  </tr>
  <tr>
    <td>IT 308: Data Mining and Data Warehousing</td>
    <td align="right">3</td>
  </tr>
</table>
</div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
</table>
</body>
</html>