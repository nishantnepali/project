<?php
	session_start();
	if(!isset($_SESSION['userdata'])){
		header('location: login.php');
	}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Dashboard page</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-default">
	  <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#"><b>Welcome to BIM Student<b></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="dashboard.php">Home</a></li>
				
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['userdata']['username']; ?> <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="#">Setting</a></li>
				<li><a href="logout.php">logout</a></li>
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
<div class="container">
	 <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Semester</th>
                  <th>University</th>
                  <th>Courses Cycle</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Semester 1</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Principles of Management</option>
                  	  <option>English Composition</option>
                  	  <option>Basic Mathematics</option>
                  	  <option>Computer Information System</option>
                  	  <option>Digital Logic<option></select></td>
                  <td><a href="assets/files/file.pdf"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>

                 <tr>
                  <td>Semester 2</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Sociology</option>
                  	  <option>Discrete Mathematics</option>
                  	  <option>Business Commnication</option>
                  	  <option>Structure Programming</option>
                  	  <option>Data Communication and Computer Network<option></select></td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>

                 <tr>
                  <td>Semester 3</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Statistics</option>
                  	  <option>Computer Ogranization</option>
                  	  <option>Data Struture and Algorithm</option>
                  	  <option>Psychology</option>
                  	  <option>Object Orientation Programming<option></select></td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>

                 <tr>
                  <td>Semester 4</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Financial Accounting</option>
                  	  <option>Micro Economics</option>
                  	  <option>Database Management System</option>
                  	  <option>Web Techonolgy</option>
                  	  <option>Micro Processor<option></select></td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>
                 <tr>
                  <td>Semester 5</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Cost and Management Accounting</option>
                  	  <option>Macro Economics</option>
                  	  <option>Computer Graphicss</option>
                  	  <option>Java Programming</option>
                  	  <option>Artificial Intellegence<option></select></td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>
                 <tr>
                  <td>Semester 6</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Business Finance</option>
                  	  <option>Operations Management</option>
                  	  <option>Principles Of Marketing</option>
                  	  <option>Client Server Computing</option>
                  	  <option>Software Engineering<option></select></td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>
                 <tr>
                  <td>Semester 7</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Critical Thinking and Decision Making</option>
                  	  <option>Ogranization Relation</option>
                  	  <option>Human Resource Management</option>
                  	  <option>Management Information System</option>
                  	  <option>Suply Chain Management</option> </select> </td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
             	</tr>
             	<tr>
                  <td>Semester 8</td>
                  <td><select><option>Tribhuwan University</option>
                  	  <option>Pokhara University</option></select></td>
                  <td><select><option>Computer Security and Cyber Law</option>
                  	  <option>Economics of Information and Communication</option>
                  	  </select></td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>
              </tbody>
            </table>
<!--
<h2> Result Table</h2>
             <div class="table-responsive table-bordered table-hover">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Semester</th>
                  <th>Faculty</th>
                  <th>Class</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>

                 <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>

                 <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>

                 <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>
                 <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>
                 <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td><a href="download.php"> <button class="btn btn-primary">Download</button></a> | <a href="marksview.php"><button class="btn btn-warning">view</button></a></td>
                </tr>
             
              </tbody>
            </table>-->
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>